<?php

/*
 * Add helper function
 */

if (!function_exists('helliomessaging')) {

    /**
     * @param string $to
     * @param string $message
     * @param array $extra_params
     * @param array $headers
     * @return mixed
     */
    function helliomessaging($to = null, $message = null, $extra_params = null, $headers = [])
    {
        $helliomessaging = app('helliomessaging');
        if (!(is_null($to) || is_null($message))) {
            return $helliomessaging->sendMessage($to, $message, $extra_params, $headers);
        }
        return $helliomessaging;
    }
}