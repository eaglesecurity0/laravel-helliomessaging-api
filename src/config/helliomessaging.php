<?php

return [

    'country_code' => '233', //Country code to be added
    'default' => env('SMS_API_DEFAULT_GATEWAY', 'HELLIO_MESSAGING'), //Choose default gateway

    // Basic Gateway Sample Configuration
    'HELLIO_MESSAGING' => [
        'params' => [
                'username' => env('HELLIO_AUTH_USERNAME'), // Your Hellio Messaging username
                'password' => env('HELLIO_AUTH_PASSWORD'), // Your Hellio Messaging password
                'senderId' => env('SENDER_ID'), // Your registered Sender ID with Hellio Messaging *Please be sure to check that the senderId is registered with us else you would receive an error.
                'messageType' => env('HELLIO_MESSAGE_TYPE'), // This is an optional param, but if you wish you can pass either 1 to send the SMS as a "Flash" message or 0 to send the message as normal SMS.
        ],
        // Optional Headers (No need to temper with them)
        'headers' => [
            'header1' => '',
            'header2' => '',
        ],
        'add_code' => false, //Include Country Code (true/false)
    ],
];
