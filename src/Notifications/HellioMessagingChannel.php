<?php

namespace HellioSolutions\HellioMessaging\Notifications;

use HellioSolutions\HellioMessaging\Exception\InvalidMethodException;
use HellioSolutions\HellioMessaging\HellioMessagingApi;
use Illuminate\Notifications\Notification;

class HellioMessagingChannel
{
    /** @var Client */
    protected $client;

    /**
     * @param HellioMessagingApi $client
     */
    public function __construct(HellioMessagingApi $client)
    {
        $this->client = $client;
    }

    /**
     * Send the given notification.
     *
     * @param mixed $notifiable
     * @param Notification $notification
     * @return void
     * @throws InvalidMethodException
     */
    public function send($notifiable, Notification $notification)
    {
        if (!$mobile = $notifiable->routeNotificationFor('sms_api')) {
            return;
        }

        $message = $notification->toHellioMessaging($notifiable);

        if (is_string($message)) {
            $message = new HellioMessagingConstructor($message);
        }

        $this->client->sendMessage($mobile, $message->content, $message->params, $message->headers);
    }
}