<?php

namespace HellioSolutions\HellioMessaging;

use Illuminate\Support\ServiceProvider;
use HellioSolutions\HellioMessaging\HellioMessagingApi;

class SmsApiServiceProvider extends ServiceProvider
{
    protected $defer = false;
    protected $configName = 'helliomessaging';

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $configPath = __DIR__ . '/config/' . $this->configName . '.php';
        $this->publishes([
            $configPath => config_path($this->configName . '.php')
        ], 'config');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $configPath = __DIR__ . '/config/' . $this->configName . '.php';
        $this->mergeConfigFrom($configPath, $this->configName);
        $this->app->bind('helliomessaging', function () {
            return new HellioMessagingApi();
        });
    }
}
