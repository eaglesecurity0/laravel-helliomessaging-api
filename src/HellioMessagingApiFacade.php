<?php
namespace HellioSolutions\HellioMessaging;
use Illuminate\Support\Facades\Facade;

class HellioMessagingApiFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'helliomessaging';
    }
}