[comment]: <> ([![Packagist]&#40;https://img.shields.io/packagist/v/eaglesecurity0/laravel-helliomessaging-api.svg&#41;]&#40;https://packagist.org/packages/eaglesecurity0/laravel-helliomessaging-api&#41; [![Packagist]&#40;https://img.shields.io/packagist/dt/eaglesecurity0/laravel-helliomessaging-api.svg&#41;]&#40;https://packagist.org/packages/eaglesecurity0/laravel-helliomessaging-api&#41; [![GitHub license]&#40;https://img.shields.io/badge/license-MIT-blue.svg&#41;]&#40;https://raw.githubusercontent.com/eaglesecurity0/laravel-helliomessaging-api/master/LICENSE&#41;)

# Integrate SMS API with Laravel

Laravel package to interact with the Hellio Messaging SMS API. HellioMessaging API channel for Laravel notifications also included.

[comment]: <> (#### [Star ⭐]&#40;https://gitlab.com/eaglesecurity0/laravel-helliomessaging-api&#41; repo to show support 😊)

## Installation

### Install Package

Require this package with composer:

```
composer require eaglesecurity0/laravel-helliomessaging-api
```

### Add Service Provider & Facade

#### For Laravel 5.5+

Once the package is added, the service provider and facade will be autodiscovered.

#### For Older versions of Laravel

Add the ServiceProvider to the providers array in `config/app.php`:

```
HellioSolutions\HellioMessaging\SmsApiServiceProvider::class,
```

Add the Facade to the aliases array in `config/app.php`:

```
'HellioMessaging': HellioSolutions\HellioMessaging\HellioMessagingFacade::class,
```

### Publish Config

Once done, publish the config to your config folder using:

```
php artisan vendor:publish --provider="HellioSolutions\HellioMessaging\SmsApiServiceProvider"
```

## Configuration

Once the config file is published, open `config/helliomessaging-api.php`

#### Global config

`country_code` : The default country code to be used

`default` : Default gateway

#### Gateway Config

Use can define multiple gateway configs like this:-

```

    // Basic Gateway Sample Configuration
    'HELLIO_MESSAGING' => [
        'params' => [
                'username' => env('HELLIO_AUTH_USERNAME'), // Your Hellio Messaging username
                'password' => env('HELLIO_AUTH_PASSWORD'), // Your Hellio Messaging password
                'senderId' => env('SENDER_ID'), // Your registered Sender ID with Hellio Messaging *Please be sure to check that the senderId is registered with us else you would receive an error.
                'messageType' => env('HELLIO_MESSAGE_TYPE'), // This is an optional param, but if you wish you can pass either 1 to send the SMS as a "Flash" message or 0 to send the message as normal SMS.
        ],
        // Optional Headers (No need to temper with them)
        'headers' => [
            'header1' => '',
            'header2' => '',
        ],
        'add_code' => false, //Include Country Code (true/false)
    ],
```

## Usage

### Direct Use

Use the `helliomessaging()` helper function or `HellioMessaging` facade to send the messages.

`TO`: Single mobile number or Multiple comma-separated mobile numbers

`MESSAGE`: Message to be sent

#### Using Helper function

- Basic Usage `helliomessaging("TO", "Message");` or `helliomessaging()->sendMessage("TO","MESSAGE");`

- Adding extra parameters `helliomessaging("TO", "Message", ["param1" => "val"]);` or `helliomessaging()->sendMessage("TO", "Message", ["param1" => "val"]);`

- Adding extra headers `helliomessaging("TO", "Message", ["param1" => "val"], ["header1" => "val"]);` or `helliomessaging()->sendMessage("TO", "Message", ["param1" => "val"], ["header1" => "val"]);`

- Using a different gateway `helliomessaging()->gateway('GATEWAY_NAME')->sendMessage("TO", "Message");`

- Using a different country code `helliomessaging()->countryCode('COUNTRY_CODE')->sendMessage("TO", "Message");`

- Sending message to multiple mobiles `helliomessaging(["Mobile1","Mobile2","Mobile3"], "Message");` or `helliomessaging()->sendMessage(["Mobile1","Mobile2","Mobile3"],"MESSAGE");`

#### Using HellioMessaging facade

- Basic Usage `HellioMessaging::sendMessage("TO","MESSAGE");`

- Adding extra parameters `HellioMessaging::sendMessage("TO", "Message", ["param1" => "val"]);`

- Adding extra headers `HellioMessaging::sendMessage("TO", "Message", ["param1" => "val"], ["header1" => "val"]);`

- Using a different gateway `HellioMessaging::gateway('GATEWAY_NAME')->sendMessage("TO", "Message");`

- Using a different country code `HellioMessaging::countryCode('COUNTRY_CODE')->sendMessage("TO", "Message");`

- Sending message to multiple mobiles `HellioMessaging::sendMessage(["Mobile1","Mobile2","Mobile3"],"MESSAGE");`

### Use in Notifications

#### Setting up the Route for Notofication

Add the method `routeNotificationForHellioMessaging()` to your Notifiable model :

```
public function routeNotificationForHellioMessaging() {
        return $this->phone; //Name of the field to be used as mobile
    }
```

By default, your User model uses Notifiable.

#### Setting up Notification

Add

`use HellioSolutions\HellioMessaging\Notifications\HellioMessagingChannel;`

and

`use HellioSolutions\HellioMessaging\Notifications\HellioMessagingMessage;`

to your notification.

You can create a new notification with `php artisan make:notification NOTIFICATION_NAME`

In the `via` function inside your notification, add `return [HellioMessagingChannel::class];` and add a new function `toHellioMessaging($notifiable)` to return the message body and parameters.

Notification example:-

```
namespace App\Notifications;

use HellioSolutions\HellioMessaging\Notifications\HellioMessagingChannel;
use HellioSolutions\HellioMessaging\Notifications\HellioMessagingMessage;
use Illuminate\Notifications\Notification;

class ExampleNotification extends Notification
{
    public function via($notifiable)
    {
        return [HellioMessagingChannel::class];
    }

    public function toHellioMessaging($notifiable)
    {
        return (new HellioMessagingMessage)
            ->content("Hello");
    }
}
```

You can also use `->params(["param1" => "val"])` to add extra parameters to the request and `->headers(["header1" => "val"])` to add extra headers to the request.

### Getting Response

You can get response by using `->response()` or get the Response Code using `->getResponseCode()`. For example, `helliomessaging()->sendMessage("TO","MESSAGE")->response();`

## Support

Feel free to post your issues in the issues section.

## Credits

Developed by [Albert Ninyeh](https://gitlab.com/eaglesecurity0 "Albert Ninyeh")

Thanks to [laravel-ovh-sms](https://gitlab.com/MarceauKa/laravel-ovh-sms "laravel-ovh-sms") & [softon-sms](https://gitlab.com/softon/sms "softon-sms")

## License

MIT
