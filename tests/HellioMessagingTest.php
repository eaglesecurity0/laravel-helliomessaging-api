<?php

namespace HellioSolutions\HellioMessaging\Tests;

use HellioMessaging;

class HellioMessagingTest extends AbstractTestCase
{

  /**
   * SendMessage Test
   *
   * @return void
   */
    public function testSendMessageResponse()
    {
        $response = HellioMessaging::sendMessage("233242813656", "Testing Hellio Messaging Laravel SMS Package")->response();
        $this->assertNotEmpty($response, "Response is empty.");
    }

    //TODO: Add more tests (https://gitlab.com/eaglesecurity0/laravel-helliomessaging-api/issues/3)
}
